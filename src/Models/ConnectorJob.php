<?php

namespace Eurofirany\ConnectorsQueue\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string connector
 * @property string method
 * @property string status
 * @property int batch
 * @mixin Builder
 */
class ConnectorJob extends Model
{

}
