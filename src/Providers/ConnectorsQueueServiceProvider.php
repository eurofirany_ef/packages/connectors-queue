<?php

namespace Eurofirany\ConnectorsQueue\Providers;

use Eurofirany\ConnectorsQueue\Console\Commands\ClearConnectorJobsCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class ConnectorsQueueServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                ClearConnectorJobsCommand::class
            ]);

            $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
                $schedule->command(ClearConnectorJobsCommand::class)->daily();
            });
        }
    }
}
