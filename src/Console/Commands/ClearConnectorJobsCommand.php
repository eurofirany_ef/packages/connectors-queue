<?php

namespace Eurofirany\ConnectorsQueue\Console\Commands;

use Eurofirany\ConnectorsQueue\Models\ConnectorJob;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ClearConnectorJobsCommand extends Command
{
    protected $signature = 'connector:jobs:clear';

    protected $description = 'Clear old connector jobs';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws Exception
     */
    public function handle()
    {
        ConnectorJob::where(
            'created_at',
            '<=',
            Carbon::now()->subDays(3)->startOfDay()
        )->delete();
    }
}
