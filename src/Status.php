<?php

namespace Eurofirany\ConnectorsQueue;

final class Status
{
    public const INIT = 'INIT';

    public const SEND = 'SEND';

    public const SUCCESS = 'SUCCESS';

    public const ERROR = 'ERROR';

    public const ERROR_NEXT_TRY = 'ERROR_NEXT_TRY';
}