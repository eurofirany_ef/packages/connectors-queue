<?php

namespace Eurofirany\ConnectorsQueue;

use Closure;
use Eurofirany\ConnectorsQueue\Models\ConnectorJob;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

/**
 * Queue for connectors
 */
class ConnectorsQueue
{
    /** @var int Code for unauthorized exception */
    private const UNAUTHORIZED_EXCEPTION_CODE = 401;

    #[ArrayShape(['class' => "string", 'function' => "string"])]
    private array $data;
    private string $connector;
    private string $method;

    private bool $withQueue;
    private int $maxTries;
    private int $maxRunningJobs;
    private int $maxWaitSeconds;
    private int $sleepSeconds;
    private int $maxPerMinute;
    private int $depth;

    private ?ConnectorJob $connectorJob;

    private int $currentTry;

    /**
     * Setting all queue parameters
     * @param bool $withQueue
     * @param int $maxTries
     * @param int $maxRunningJobs
     * @param int $maxWaitSeconds
     * @param int $sleepSeconds
     * @param int $maxPerMinute
     * @param int $depth
     * @return $this
     */
    public function setting(
        bool $withQueue = false,
        int  $maxTries = 3,
        int  $maxRunningJobs = 5,
        int  $maxWaitSeconds = 300,
        int  $sleepSeconds = 10,
        int  $maxPerMinute = 0,
        int  $depth = 5
    ): static
    {
        $this->withQueue = $withQueue;
        $this->maxTries = $maxTries;
        $this->maxRunningJobs = $maxRunningJobs;
        $this->maxWaitSeconds = $maxWaitSeconds;
        $this->sleepSeconds = $sleepSeconds;
        $this->depth = $depth;
        $this->maxPerMinute = $maxPerMinute;

        return $this;
    }

    /**
     * Set with queue parameter to enable or disable queue
     * @param bool $withQueue
     */
    public function setWithQueue(bool $withQueue = false)
    {
        $this->withQueue = $withQueue;
    }

    /**
     * Call connector
     * @param Closure $closure Connector method
     * @return mixed
     * @throws Exception
     */
    public function call(Closure $closure): mixed
    {
        if ($this->withQueue) {
            $this->init();

            $this->waitIfYouHaveTo();

            return $this->callWithQueue($closure);
        }

        return $closure();
    }

    /**
     * If connector need wait, wait
     * @return bool
     */
    private function waitIfYouHaveTo(): bool
    {
        if ($this->isTooManyJobs()) {
            $this->wait();

            return $this->waitIfYouHaveTo();
        }

        return true;
    }

    /**
     * Check if there is too many jobs
     * @return bool
     */
    private function isTooManyJobs(): bool
    {
        return $this->isTooManyRunningJobs() || $this->isTooManyJobsPerMinute();
    }

    /**
     * Check if there is too many running currently jobs
     * @return bool
     */
    private function isTooManyRunningJobs(): bool
    {
        return ConnectorJob::where('connector', $this->connector)
                ->where('id', '!=', $this->connectorJob->id)
                ->where('created_at', '>=', Carbon::now()->subSeconds($this->maxWaitSeconds))
                ->where('status', '!=', STATUS::SUCCESS)
                ->where('status', '!=', STATUS::ERROR)
                ->count() >= $this->maxRunningJobs;
    }

    /**
     * Check if there is too many jobs runned per minute
     * @return bool
     */
    private function isTooManyJobsPerMinute(): bool
    {
        return $this->maxPerMinute !== 0 && ConnectorJob::where('connector', $this->connector)
                ->where('id', '!=', $this->connectorJob->id)
                ->where('created_at', '>=', Carbon::now()->subMinute())
                ->count() >= $this->maxPerMinute;
    }

    /**
     * Call connector with queue
     * @param Closure $closure Connector method
     * @throws Exception
     */
    private function callWithQueue(Closure $closure): mixed
    {
        $this->currentTry++;

        $this->setStatus(STATUS::SEND);

        try {
            $response = $closure();

            $this->setStatus(STATUS::SUCCESS);

            return $response;
        } catch (Exception $e) {
            if ($this->isItForNextTry($e)) {
                $this->setStatus(STATUS::ERROR_NEXT_TRY);

                $this->wait();

                return $this->callWithQueue($closure);
            }

            $this->setStatus(STATUS::ERROR);

            throw $e;
        }
    }

    /**
     * Will connector make next try
     * @param Exception $e Exception from connector
     * @return bool
     */
    #[Pure] private function isItForNextTry(Exception $e): bool
    {
        return $e->getCode() != self::UNAUTHORIZED_EXCEPTION_CODE && $this->currentTry <= $this->maxTries;
    }

    /**
     * Init service
     */
    private function init()
    {
        $this->prepareData();

        $this->prepareConnectorAndMethod();

        $this->resetParams();

        $this->setStatus(Status::INIT);
    }

    /**
     * Reset service params
     */
    private function resetParams()
    {
        $this->currentTry = 0;

        $this->connectorJob = null;
    }

    /**
     * Prepare data for service
     */
    private function prepareData()
    {
        $this->data = Arr::last(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $this->depth));
    }

    /**
     * Prepare current connector and called method
     */
    private function prepareConnectorAndMethod()
    {
        $this->connector = $this->data['class'];
        $this->method = $this->data['function'];
    }

    /**
     * Set connector job status
     * @param string $status New status for job
     */
    private function setStatus(string $status)
    {
        if ($this->connectorJob === null)
            $this->initConnectorJob();

        $this->connectorJob->status = $status;
        $this->connectorJob->batch = $this->currentTry;

        $this->connectorJob->save();
    }

    /**
     * Create new ConnectorJob instance for this job
     */
    private function initConnectorJob()
    {
        $this->connectorJob = new ConnectorJob();

        $this->connectorJob->connector = $this->connector;
        $this->connectorJob->method = $this->method;
    }

    /**
     * Call sleep for connector
     */
    private function wait()
    {
        sleep($this->sleepSeconds);
    }
}